package fr.usmb.m2isc.javaee.comptes.jpa;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@NamedQueries ({
        @NamedQuery(name="allAgency", query="SELECT a FROM Agence a"),
})
@Entity
public class Agence implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;

    private String nameAgence;

    @OneToOne
    private Backlog backlog;


    public Agence() {}

    public Agence(String nameAgence) {
        this.nameAgence = nameAgence;
    }

    public int getId() {
        return id;
    }

    public String getNameAgence() {
        return nameAgence;
    }

    public void setNameAgence(String nameAgence) {
        this.nameAgence = nameAgence;
    }

    public void setBacklog(Backlog backlog) {
        this.backlog = backlog;
    }

    public int getBacklogid() {
        return backlog.getId();
    }

    public Backlog getBacklog() {
        return this.backlog;
    }
}

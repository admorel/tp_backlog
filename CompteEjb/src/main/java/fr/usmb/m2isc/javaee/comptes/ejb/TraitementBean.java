package fr.usmb.m2isc.javaee.comptes.ejb;


import fr.usmb.m2isc.javaee.comptes.jpa.*;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Stateless
@Remote
public class TraitementBean implements Traitement {

    @PersistenceContext
    private EntityManager em;

    public TraitementBean() {
    }

    //Opération lié a l'utilisateur
    public Utilisateur newUtilisateur(String nameUser)
    {
        Utilisateur user = new Utilisateur(nameUser);
        em.persist(user);
        return user;
    }

    public Utilisateur getUtilisateur(int id)
    {
        return em.find(Utilisateur.class, id);
    }

    @Override
    public List<Utilisateur> findAllUtilisateur() {
        Query req = em.createNamedQuery("allUser");
        return req.getResultList();
    }

    @Override
    public Utilisateur findUtilisateur(String userName) {
        Query req = em.createNamedQuery("findUserWithName");
        req.setParameter("nom", userName);
        int resTemp = req.getFirstResult();
        return getUtilisateur(resTemp);
    }


    //Opération lié a l'agence
    public Agence newAgence(String nameAgence)
    {
        Agence agency = new Agence(nameAgence);
        em.persist(agency);
        return agency;
    }


    public Agence getAgence(int id)
    {
        return em.find(Agence.class, id);
    }


    @Override
    public List<Agence> findAllAgence() {
        Query req = em.createNamedQuery("allAgency");
        return req.getResultList();
    }


    //Opération lié aux entrées de backlog


    public Backlog newBacklog()
    {
        Backlog backlog = new Backlog();
        em.persist(backlog);
        return backlog;
    }

    public Backlog getBacklog(int id)
    {
        return em.find(Backlog.class, id);
    }

    public void lierBacklogToAgency(int idAgency, int idBacklog)
    {
        Agence agc = getAgence(idAgency);
        Backlog blg = getBacklog(idBacklog);
        agc.setBacklog(blg);
    }



    public List<Entree> getEntrees(int idBacklog)
    {
        Backlog backlog = getBacklog(idBacklog);
        return backlog.getEntreeBacklog();
    }



    public Entree newEntry(String nameEntree, int priorite, int estimation, String description, Backlog backlog)
    {
        Entree entry = new Entree(nameEntree, priorite, estimation, description, backlog);
        em.persist(entry);
        return entry;
    }

    public void deleteEntry(Agence agency, int idEntree)
    {

        agency.getBacklog().getEntreeBacklog().remove(idEntree);
    }

    public Entree getEntry(int id)
    {
        return em.find(Entree.class, id);
    }

    public void modifyPrioriteEntry(int id, int newPriorite)
    {
        Entree entry = getEntry(id);
        entry.setPriorite(newPriorite);
    }

    public void modifyEstimationEntry(int id, int newEstimation)
    {
            Entree entry = getEntry(id);
            entry.setEstimation(newEstimation);
    }

    public void modifyDescriptionEntry(int id, String newDescription)
    {
        Entree entry = getEntry(id);
        entry.setDescription(newDescription);
    }

    @Override
    public List<Entree> findAllEntree(int idBacklog) {
        Backlog b = getBacklog(idBacklog);
        Query req = em.createNamedQuery("allEntryBacklog");
        req.setParameter("backlog", b);
        return req.getResultList();
    }

    //Opération lié au commentaire
    public Commentaire newCommentaire(String commentaire, Utilisateur user, Entree entree)
    {
        Commentaire com = new Commentaire(commentaire, user, entree);
        em.persist(com);
        return com;
    }

    public Commentaire getCommentaire(int id)
    {
        return em.find(Commentaire.class, id);
    }

    public void modifyCommentaire(int id, String newCommentaire)
    {
        Commentaire com = getCommentaire(id);
        com.setCommentaire(newCommentaire);
    }


    public List<Commentaire> findAllCommentaireFromEntry(int idEntry) {
        Entree e = getEntry(idEntry);
        Query req = em.createNamedQuery("allCommentaryEntry");
        req.setParameter("entreeCourante", e);
        return req.getResultList();
    }
}

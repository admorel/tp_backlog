package fr.usmb.m2isc.javaee.comptes.jpa;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Backlog implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;

    @OneToMany
    private List<Entree> entreeBacklog;

    public Backlog ()
    {
        entreeBacklog = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void ajouterNouvelleEntree(Entree entry){
        this.entreeBacklog.add(entry);
    }

    public List<Entree> getEntreeBacklog() {
        return entreeBacklog;
    }
}

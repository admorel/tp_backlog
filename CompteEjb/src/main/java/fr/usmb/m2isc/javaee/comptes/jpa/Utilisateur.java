package fr.usmb.m2isc.javaee.comptes.jpa;


import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@NamedQueries({
        @NamedQuery(name="allUser", query="SELECT u FROM Utilisateur u"),
        @NamedQuery(name="findUserWithName", query="SELECT u FROM Utilisateur u WHERE u.nameUser=:nom")
})
@Entity
public class Utilisateur implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;

    private String nameUser;

    @OneToMany
    private List<Commentaire> commentaire;

    public Utilisateur(){}

    public Utilisateur(String nameUser)
    {
        this.nameUser = nameUser;
    }

    public int getId() {
        return this.id;
    }

    public String getNameUser() {
        return this.nameUser;
    }

    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Commentaire> getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(List<Commentaire> commentaire) {
        this.commentaire = commentaire;
    }
}

package fr.usmb.m2isc.javaee.comptes.web;

import fr.usmb.m2isc.javaee.comptes.ejb.Traitement;
import fr.usmb.m2isc.javaee.comptes.jpa.*;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet("/CreerCommentaireEntreeServlet")
public class CreerCommentaireEntreeServlet extends HttpServlet{

    @EJB
    private Traitement trt;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreerCommentaireEntreeServlet()
    {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        int idEntry = Integer.parseInt(request.getParameter("idEntry"));
        String user = request.getParameter("user");
        String commentaire = request.getParameter("commentaire");

        Entree entree = trt.getEntry(idEntry);
        Utilisateur utilisateur = trt.findUtilisateur(user);

        if(utilisateur == null)
        {
            utilisateur = trt.newUtilisateur(user);
        }

        Commentaire com = trt.newCommentaire(commentaire, utilisateur, entree);

        List<Commentaire> listCom = trt.findAllCommentaireFromEntry(idEntry);


        request.setAttribute("entry", entree);
        request.setAttribute("listCom", listCom);

        request.getRequestDispatcher("/AfficherEntree.jsp").forward(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}

package fr.usmb.m2isc.javaee.comptes.web;

import fr.usmb.m2isc.javaee.comptes.ejb.Traitement;
import fr.usmb.m2isc.javaee.comptes.jpa.Agence;
import fr.usmb.m2isc.javaee.comptes.jpa.Commentaire;
import fr.usmb.m2isc.javaee.comptes.jpa.Entree;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/AfficherEntreeBacklogServlet")
public class AfficherEntreeBacklogServlet extends HttpServlet {
    @EJB
    private Traitement trt;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AfficherEntreeBacklogServlet() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        int idEntre = Integer.parseInt(request.getParameter("idEntre"));

        Entree entry = trt.getEntry(idEntre);

        List<Commentaire> listCom = trt.findAllCommentaireFromEntry(idEntre);

        request.setAttribute("entry", entry);
        request.setAttribute("listCom", listCom);


        request.getRequestDispatcher("/AfficherEntree.jsp").forward(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}

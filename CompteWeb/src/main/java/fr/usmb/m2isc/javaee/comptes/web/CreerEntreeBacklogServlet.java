package fr.usmb.m2isc.javaee.comptes.web;

import fr.usmb.m2isc.javaee.comptes.ejb.Traitement;
import fr.usmb.m2isc.javaee.comptes.jpa.Agence;
import fr.usmb.m2isc.javaee.comptes.jpa.Backlog;
import fr.usmb.m2isc.javaee.comptes.jpa.Entree;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/CreerEntreeBacklogServlet")
public class CreerEntreeBacklogServlet extends HttpServlet {

    @EJB
    private Traitement trt;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreerEntreeBacklogServlet()
    {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        int idAgence = Integer.parseInt(request.getParameter("idAgence"));
        Agence agence = trt.getAgence(idAgence);

        String nameEntree = request.getParameter("nameEntree");
        int priorite = Integer.parseInt(request.getParameter("priorite"));
        int estimation = Integer.parseInt(request.getParameter("estimation"));
        String description = request.getParameter("description");
        int idBacklog = Integer.parseInt(request.getParameter("idBacklog"));

        Backlog backlog = trt.getBacklog(idBacklog);
        Entree entry = trt.newEntry(nameEntree, priorite, estimation, description, backlog);

        List<Entree> listEntry = trt.findAllEntree(idBacklog);


        request.setAttribute("agence", agence);
        request.setAttribute("listEntry", listEntry);
        request.setAttribute("idBacklog", idBacklog);

        request.getRequestDispatcher("/AfficherBacklogAgence.jsp").forward(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}

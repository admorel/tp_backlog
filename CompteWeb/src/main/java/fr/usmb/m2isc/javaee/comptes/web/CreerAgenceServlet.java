package fr.usmb.m2isc.javaee.comptes.web;

import fr.usmb.m2isc.javaee.comptes.ejb.Traitement;
import fr.usmb.m2isc.javaee.comptes.jpa.Agence;
import fr.usmb.m2isc.javaee.comptes.jpa.Backlog;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/CreerAgenceServlet")
public class CreerAgenceServlet extends HttpServlet{

    @EJB
    private Traitement trt;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public CreerAgenceServlet()
    {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String agencyName = request.getParameter("agenceName");

        Agence newAgence = trt.newAgence(agencyName);
        Backlog bcl = trt.newBacklog();
        trt.lierBacklogToAgency(newAgence.getId(),bcl.getId());

        request.setAttribute("agence", newAgence);

        request.getRequestDispatcher("/AfficherChoixAgence.jsp").forward(request, response);
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}

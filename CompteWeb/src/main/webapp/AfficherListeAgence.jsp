<%--
  Created by IntelliJ IDEA.
  User: adrien
  Date: 07/11/18
  Time: 13:27
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Liste des agences</title>
</head>
<body>
    <h1>Liste des agences</h1>

    <c:forEach items="${listeAgence}"  var="agence">
        <p>Nom : ${agence.nameAgence}</p>
        <p>id : ${agence.id}</p>

        <form action="AfficherBacklogAgenceServlet" method="post">
            <input type="hidden" name="idAgency" value="${agence.id}">
            <input type="submit" value="Voir Backlog">
        </form>
    </c:forEach>

    <p><a href="index.html" target="_blank">Revenir  la page principale</a></p>
</body>
</html>

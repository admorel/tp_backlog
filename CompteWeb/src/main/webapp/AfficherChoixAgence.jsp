<%--
  Created by IntelliJ IDEA.
  User: adrien
  Date: 12/11/18
  Time: 13:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Choix agence</title>
</head>
<body>
    <h1>Agence</h1>

    <h2>Nouvel agence :</h2>
    <form action="CreerAgenceServlet" method="post">
        Nom : <input type="text" name="agenceName">
        <input type="submit" value="valider">
    </form>

    <h2>Voir liste agence :</h2>
    <form action="AfficherListeAgenceServlet" method="post">
        <input type="submit" value="Voir liste des agences">
    </form>

    <p><a href="index.html" target="_blank">Revenir  la page principale</a></p>
</body>
</html>

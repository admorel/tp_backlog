<%--
  Created by IntelliJ IDEA.
  User: adrien
  Date: 20/11/18
  Time: 23:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="d"%>
<html>
<head>
    <title>Afficher Backlog Agence</title>
</head>
<body>

    <h1> Agence ${agence.nameAgence}</h1>

    <h2> Backlog</h2>

    <c:forEach items="${listEntry}"  var="entree">
        <h4>Entrée: ${entree.nameEntree}</h4>
        <form action="AfficherEntreeBacklogServlet" method="post">
            <input type="hidden" name="idEntre" value="${entree.id}">
            <input type="submit" value="Voir entrée">
        </form>
    </c:forEach>

    <h2> Nouvelle entrée : </h2>

    <form action="CreerEntreeBacklogServlet" method="post">
        Nom : <input type="text" name="nameEntree">
        Priorite : <input type="text" name="priorite">
        Estimation : <input type="text" name="estimation">
        Description : <input type="text" name="description">
        <input type="hidden" name="idBacklog" value="${idBacklog}">
        <input type="hidden" name="idAgence" value="${agence.id}">
        <input type="hidden" name="listEntry" value="${listEntry}">
        <input type="submit" value="Créer entrée">
    </form>

    <a href="index.html" target="_blank">Revenir  la page principale</a>

</body>
</html>

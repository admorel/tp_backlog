# Compte rendu TP2 JEE Backlog

Dans ce TP, nous avons mis en place une gestion de backlog pour différentes agences. 

Nous pouvons :
- Créer des agences,
- Afficher toutes les agences,
- Choisir une agence,
- Afficher son backlog,
- Choisir une entrée,
- Afficher l'entrée,
- Rajouter un commentaire.

Ce que l'on ne peut pas faire:
- Supprimer une agence,
- Supprimer une entrée,
- Supprimer un commentaire,
- Connecter un utilisateur pour le mettre comme auteur par défaut des commentaires.


Pour tester le projet, il faut récupérer les sources sur le gitlab suivant: https://gitlab.com/fararn/tp_backlog


Pour déployer le projet, il faut un serveur WildFly ou GlassFish, et déployer l'archie CompteEAR.ear sur un serveur 
JavaEE 7
(Pour les serveurs GlassFish, penser à démarrer la BDD à coté).


Niveau arborescence:

- Les objets EJB sont stockés dans l'archive CompteEjb
    -> L'EJB session est stocké dans CompteEjb/src/main/java/fr.usmb.m2.isc.javaee.comptes/ejb/
    -> Les objets JPA sont stockés dans CompteEjb/src/main/java/fr.usmb.m2.isc.javaee.comptes/jpa/

- L'application est dans l'archive CompteWeb
    -> Dans CompteWeb/src/main/java/fr.usmb.m2.isc.javaee.compte.web/ sont stockés les servlets
    -> Dans CompteWeb/src/main/webapp sont stockés les pages html et jsp.

